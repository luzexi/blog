---
layout: page
title: 程序员必需品
---

- - -

## 必需品①

![Cambly-speek-with-nativer](/assets/engineer-shopping/cambly/cambly1.png)

![Cambly-2018](/assets/engineer-shopping/cambly/cambly2.png)

###### Cambly是我见过的所有英语口语练习方式中最棒的在线交流中心了，它可以让我随时随地能和外国人练习英语，关键是便宜啊，又能学好英文又不多掏钱。

最低可以到大约50块钱/30分钟的样子，比起外面动不动就几万块的XXX英语培训机构要好几百倍。

Cambly中的教师都是纯正的英语母语口音的本地人，而且几乎所有人都有教师资格证，男老师女老师都有，可以在线随你挑选，评分，聊天，发信息等。

倘若不想以上课的形式练习英语，完全可以不用上课，您可以单纯就用英语聊天，聊天时不知道该怎么说，老师还会帮你纠正，聊旅游，聊学习，聊娱乐，聊电影，聊生活等等等，老师们准备了N多个话题等你来。

从这里可以获取免费对话时间，方式如下：

###### 1。 点击连接并立即注册， [老外一对一英语交流，免费获取15分钟对话时间](https://www.cambly.com/en?&lang=zh_CN&referralCode=jesseenglish#referral)

###### 2。 [Cambly老外一对一英语交流](https://www.cambly.com/en?&lang=zh_CN&referralCode=jesseenglish#referral) 点击链接注册账号后，在推荐码中输入 JesseEnglish 后，点击确定(可能是’应用‘)按钮。

![cambly-setting](/assets/engineer-shopping/cambly/cambly4.png)

注册完成设置后就能获得免费的15分钟的在线与老外交流的时间了，回到Cambly的主页，就能选择自己喜欢的老外进行在线交流或上课啦。

<!-- ![cambly-setting](/assets/engineer-shopping/cambly/cambly3.png) -->

- - -

## 必需品②

正在提炼中....


### 如有疑问请留言

{% include comments.html %}
