---
layout: post
status: publish
published: false
title: 《Unity3D高级编程之进阶主程》第一章，C#要点技术(五)
description: "unity3d 高级编程 c# 主程 算法 设计模式 动态库 底层算法"
excerpt_separator: ===
tags:
- 书籍著作
- Unity3D
- 前端技术
---

#第一章，C\#要点技术(五)

前文回顾 [《Unity3D高级编程之进阶主程》第一章，C#要点技术(一)](http://)

很多老鸟看到C#基础总想跳过，因为看了太多次，次次都一样，有时候看得都想吐，基础里无非是几个语法，或者由继承展开的特性，再加上一些高级特有的属性，看多了当然会吐。而我在这里想写些不一样的东西，我认为能看这本书的，基本上都能做到基础的语法部分已经滚瓜烂熟了。所以我们在基础的语法之上讲些更高级的东西会来得更有趣些，比如算法设计，比如常用组件的底层代码分析，比如设计模式，比如动态库(so文件和dll文件)等等。

===

2。 ## 算法设计

### 快速排序
http://www.cnblogs.com/eniac12/p/5329396.html

### 归并排序
http://www.cnblogs.com/eniac12/p/5329396.html

### 堆排序
http://www.cnblogs.com/eniac12/p/5329396.html
https://blog.csdn.net/coolwriter/article/details/78732728

### 拓扑排序
https://blog.csdn.net/dylan_frank/article/details/52292518

### 非常用排序，计数排序，基数排序，桶排序
https://blog.csdn.net/coolwriter/article/details/78732728

### 广度优先搜索
https://blog.csdn.net/dylan_frank/article/details/52292518

### 深度优先搜索
https://blog.csdn.net/dylan_frank/article/details/52292518

### Dijkstra最短路径算法
http://www.cnblogs.com/biyeymyhjob/archive/2012/07/31/2615833.html

### A星非最优最短路径算法
https://blog.csdn.net/qq_21120027/article/details/52949157

### 树状数组
https://blog.csdn.net/qq_21120027/article/details/50585002

### KMP字符串匹配算法
https://blog.csdn.net/qq_21120027/article/details/50911370

### 凹行包边算法



