---
layout: page
title: About Me
---
姓名：陆泽西 (Jesse Lu)

Industry ：Game Develop

Position ：Shanghai

### Game Project：

		《使命召唤:围攻》 3D COC战争塔防

		《使命召唤:英雄》 3D COC战争塔防

		《白猫计划》 3D ARPG 2014 – 2015

		《临兵斗者三国志》3D 回合制RPG游戏  2013 – 2014

		《王途霸业》2D 战争策略游戏  2012 – 2013

		《凡人修仙》3D PC 单机游戏  2011 – 2012

		《公元》3D ARPG游戏  2010 – 2011

		《星月精灵》3D 角色扮演类游戏  2010

		《汽车使命》3D 赛车竞技游戏  2009


### Keep moving, Never stop.

		不开口没有人知道你想要什么;

		不去做任何想法都只在脑海里游泳;

		不迈出脚步，永远找不到你前进的方向;


{% include gongzhonghao.html %}

开源地址: [https://github.com/luzexi](https://github.com/luzexi)

EMAIL：jesse_luzexi@163.com

QQ:16008977 (请标注为什么要加)

主程群: 334097846 (只有主程以上级别才会被审核通过)

{% include paypal_donation.html %}